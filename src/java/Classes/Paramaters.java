/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MajorKingpin
 */
public class Paramaters {
     private static final List<String> commedy = new ArrayList<String>() {{
                add("Mrs.+Brown%27s+Boys");add("frankie+boyle");add("Mario+Rosenstock");
                add("The+Rubberbandits");add("peter+kay");add("jimmy+carr");add("tommy+tiernan");
                add("pat+shortt");add("keith+barry");add("brendan+grace");add("Ardal+O%27Hanlon");
                add("Jason+Byrne");add("Neil+Delamere");add("Jersey+Boys");add("Des+Bishop");add("Kevin+Hart");
                add("Rob+Delaney");add("Russell+Howard");add("Joe+Lycett");add("Ross+O%27Carroll-Kelly");
                add("Conal+Gallen");add("Tom+Stade");add("Reginald+D+Hunter");add("Sarah+Millican");
                add("Louis+C.K.");add("Amy+Schumer");add("Jack+Whitehall");add("Katherine+Jenkins");
                add("Riverdance");add("Chris+Ramsey");add("Vodafone+Comedy+Festival");add("Bill+Burr");
                add("Billy+Elliot+the+Musical");add("Al+Porter");add("Bill+Bailey");add("Ed+Byrne");
                add("Rubberbandits");add("Romesh+Ranganathan");add("Sean+Lock");add("Derren+Brown");
            }};
        
        private static final List<String> sport = new ArrayList<String>() {{
                add("rugby");add("golf");add("ufc");
                add("football");add("soccer");add("snooker");add("boxing");
                add("liverpool");add("man+utd");add("arsenal");add("chelsea");
                add("leinster");add("munster");add("racing");add("tennis");add("gaa");
                add("gaelic");add("barcelona");add("cheltenham");add("leopardstown");   
                add("World Cup");add("Euros");
        }};
        
        private static final List<String> music = new ArrayList<String>() {{
                add("rihanna");add("Electric+Picnic");add("Rod+Stewart");
                add("Macklemore");add("Def+Leppard");add("Bryan+Adams");add("Andre+Rieu");
                add("Mumford+%26+Sons");add("Christy Moore");add("Kodaline");add("Ryan+Sheridan");
                add("Nathan+Carter");add("Massive+Attack");add("The+Corrs");add("Paddy+Casey");add("Slipknot");         
        }};
        
        private static final List<String> jobs = new ArrayList<String>() {{
                add("java");add("front+end");add("junior+developer");
                add("technical support");add("test+engineer");add("back+end");add("installations+assistant");
                add("service+desk+analyst");add("it+service");add("software+tester");add("Payroll+Specialist");
                add("database");add("data+analyst");        
        }};
        
        public List<String> comedyReturn() {     
            return commedy;
        }
        
        public List<String> sportsReturn() {     
            return sport;
        }
        
        public List<String> musicReturn() {     
            return music;
        }
        
        public List<String> jobsReturn() {     
            return jobs;
        }
}
