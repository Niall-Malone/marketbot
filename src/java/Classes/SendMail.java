package Classes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class SendMail {

    final String username = "BOT EMAIL LOGIN HERE";
    final String password = "BOT PASSWORD HERE";
    
    
    public Session auttenticate() {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });
        
        return session;
    }
    

    public void sendEmail(Session s, String msg, String recip) {
        try {
            createDocReport(msg);
            System.out.println("Sending!!!");
            Message message = new MimeMessage(s);
            message.setFrom(new InternetAddress("BOT EMAIL LOGIN HERE"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(recip));
            message.setSubject("WebApp AlertSMS");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart(); 
            messageBodyPart.setText("--:Tickets Completed:--\r\n");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "C:\\test\\market.html";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void createDocReport(String msg) {
        try {
                File file = new File("C:\\test\\market.html");

		// if file doesnt exists, then create it
                    if (!file.exists()) {
			file.createNewFile();
                    }
                    
                    deleteFile("C:\\test\\market.html");
                    
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write("<html>");
                    bw.write("<body>");
                    bw.write("<style>table, th, td { border: 2px solid black;}</style>");
                    bw.write(writeSearchScript());
                    bw.write("<table id='dTable' style='width:100%; text-align:center; font-size: 20px;'>");
                    bw.write("<tr><th style='width: 30%;'>Description :</th><th style='width: 10%;'>Domain :</th><th width: 10%;>Price :</th><th style='width: 30%;'>link :</th></tr>");             
                    bw.write(msg);
                    bw.write("</table><br><br>");
                    bw.write("</body>");
                    bw.write("</html>");
                    bw.close();

            } catch (IOException e) {
		e.printStackTrace();
            }
        
        
    }
    
    public String writeSearchScript(){
        String output = "";
        output = 
        "<label for='sortdesc'></label>Sort By Description : <input id='sortdesc' name='sortdesc' "+
        "value='' oninput='srchF(this.value)'><br><br><script>function srchF(input) {"+
        "var rowCT = document.getElementById('dTable').rows.length;var table = document.getElementById('dTable');"+
        "var value = '';var row;var selector;var cells;for ( var i = 1; i < rowCT; i++ ) {selector= 'row' + i;"+
	"if ( document.getElementById(selector).style.display === '' ) {try {"+
        "cell = document.getElementById(selector).cells[0].innerHTML;row = document.getElementById(selector);"+
	"if ( cell.toLowerCase().indexOf(input.toLowerCase()) <= -1 ) {row.style.display = 'none';}"+
        "} catch(err) {}} else if ( input === '' ) {document.getElementById(selector).style.display = '';"+
	"}}}</script>";
        return output;
    }

    
    public static boolean renameFileExtension(String source, String newExtension)
    {
    
        String target;
        String currentExtension = getFileExtension(source);

        if (currentExtension.equals("")){
            target = source + "." + newExtension;
        }
        else {
            target = source.replaceFirst(Pattern.quote("." +
            currentExtension) + "$", Matcher.quoteReplacement("." + newExtension));
        }
    
        return new File(source).renameTo(new File(target));
  }

  public static String getFileExtension(String f) {
    String ext = "";
    int i = f.lastIndexOf('.');
    if (i > 0 &&  i < f.length() - 1) {
      ext = f.substring(i + 1);
    }
    return ext;
  }
 
  public void deleteFile(String src) {
      try{	
          File file = new File(src);
            if(file.delete()){}else{System.out.println("Delete operation has failed.");}   
    	}catch(Exception e){
    		e.printStackTrace();    		
    	}
  }
}