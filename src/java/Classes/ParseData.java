package Classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Niall
 */
public class ParseData {
    
    static int cutPosA = 0;
    static int cutPosB = 0;
   
    // This is the process for Adverts.ie
    // Searching 4 tickets
    

    public Elements getElements(String query) throws InterruptedException {
        Document doc = null;
        Elements data = null;
   
            String conStr = "http://www.adverts.ie/for-sale/tickets/62/q_" + query;
            try {
                doc = Jsoup.connect(conStr).userAgent("Mozilla/5.0 ssss (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
            } catch (IOException ex) {
                Logger.getLogger(ParseData.class.getName()).log(Level.SEVERE, null, ex);
            }
        
            data = doc.select(".info");
        
            return data;
    }

    public Elements getElements3(String query) throws InterruptedException {
        Document doc = null;
        Elements data = null;
            
            String conStr = "http://www.adverts.ie/jobs/3/q_" + query + "/";
            try {
                doc = Jsoup.connect(conStr).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
            } catch (IOException ex) {
                Logger.getLogger(ParseData.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //.search_result.info-box.job_listing
            data = doc.select(".search_result.info-box.job_listing ");
        
            return data;
    }
    
    public Elements getElements2(String query, int n) throws InterruptedException {
        Document doc = null;
        Elements data = null;
        String conStr = ""; 
                
            if ( n == 1 ) {
                conStr = "http://www.donedeal.ie/find/othertickets/for-sale/Ireland/" + query;
            } else if ( n == 2 ) {
                conStr = "http://www.donedeal.ie/find/musictickets/for-sale/Ireland/" + query;
            } else if ( n == 3 ) {
                conStr = "http://www.donedeal.ie/find/sporttickets/for-sale/Ireland/" + query;
            }

            try {
                doc = Jsoup.connect(conStr).userAgent("Mozilla/5.0 ssss (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
            } catch (IOException ex) {
                Logger.getLogger(ParseData.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            data = doc.select("body");

            return data;
    }
    
    public Elements getElements4(String query) {
        Document doc = null;
        Elements data = null;
   
        Elements list = null;
        
            String conStr = "http://jobs.donedeal.ie/jobs?agencyOn=true&employmentType=Permanent&employmentType=Temporary&employmentType=Contract&employmentType=Seasonal&category=it-and-programming-jobs&workingHours=Full-time&workingHours=Part-time&words=" + query + "&agencyOn=true";
            try {
                doc = Jsoup.connect(conStr).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
            } catch (IOException ex) {
                Logger.getLogger(ParseData.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            data = doc.select(".ad");

            return data;
    }
    
    public List<Tickets> getList(Elements d) {
        List<Tickets> retturnList = new ArrayList<>();
        
            Elements dt2 = d.select(".header");
            Elements dt3 = d.select(".item_description");
            Elements dt4 = d.select("h4");
            Tickets tic = null;
            
            if ( dt2.size() == dt3.size() && dt2.size() == dt4.size()) {
                String link = "";
                String price = "";
                String desc = "";
                String lnk = "";


                for ( int i = 0; i < dt2.size(); i++ ) {
                    // this block gets all the prices
                    // cleans of whitespace
                    link = dt2.get(i).toString();
                    cutPosA = link.indexOf("<dd>");
                    cutPosB = link.indexOf("</dd>");
                    link = link.substring(cutPosA + 4, cutPosB );
                    link = link.replaceAll("\\s","");
                    price = link;    
                    
                    // this block gets all the descriptions
                    // removing irrelevant whitespace
                    link = dt3.get(i).toString();
                    cutPosA = link.indexOf("<div class=");
                    cutPosB = link.indexOf("</div>");
                    link = link.substring(cutPosA + 33, cutPosB -1);
                    desc = link;

                    // this is getting link
                    // clearing irrelevant space!!!
                    link = dt4.get(i).toString();
                    cutPosA = link.indexOf("href=");
                    cutPosB = link.indexOf(" title");
                    link = link.substring(cutPosA + 6, cutPosB - 1);
                    lnk = link;
                    
                    tic = new Tickets("Adverts",price,desc,"http://www.adverts.ie" + lnk);
                    
                    retturnList.add(tic);
                }
                
            }
            
            
        return retturnList;
    }
    
    public List<Jobs> getListJobs(Elements d) {
        List<Jobs> retturnList = new ArrayList<>();
        
            Elements dt3 = d.select(".item_description");
            Elements dt4 = d.select("h4");
            Jobs j = null;
            String chk = "";
            
            if ( dt3.size() == dt4.size()) {
                String link = "";
                String title = "";
                String desc = "";
                Elements e = null;
                
                for ( int i = 0; i < dt3.size(); i++ ) {
                    // this block is for description!!
                    chk = dt3.get(i).toString();
                    cutPosA = chk.indexOf("<div class=");
                    cutPosB = chk.indexOf("</div>");
                    chk = chk.substring(cutPosA + 33, cutPosB -1);
                    desc = chk;

                    // this block is for link and title
                    chk = dt4.get(i).toString();
                    e = dt4.get(i).select("a");
                    link = e.attr("href");
                    title = e.attr("title");

                    j = new Jobs(title,desc,"http://www.adverts.ie" + link);
                    retturnList.add(j);
                }
                
            }
            
        return retturnList;
    }
    
    public List<Tickets> parseDonedeal(String input) {
        List<Tickets> myList = new ArrayList<>();
        
        int posA = input.indexOf("window.searchResults");
        int posB = input.indexOf("window.searchFilters");
                
        String filtStr = input.substring(posA, posB);
        //System.out.println("input ::: " + filtStr);
        
        // start cut position
        int cutPosA = 0;
        // end cut position
        int cutPosB = 0;
        // Exit position for header()
        int cutPosC = 0;
        
        // 4 things needed for Ticket obj [ domain, price, desc, link ] 
        
        String pric = "";
        String desc = "";
        String lnk = "";
        
        String findPrice = "";
        Tickets t = null;
        boolean flag = false;
        
        int chk1 = filtStr.indexOf("privateCount")+14;
        int chk2 = filtStr.indexOf("tradeCount")-2;
        String chkStr = filtStr.substring(chk1, chk2);
        String chkStr2 = "";
        int str2Parse = 2;
        String temp = "";
        
        if ( !chkStr.equals("0")) {
            
        while ( flag == false) {
        
            cutPosA = filtStr.indexOf("age") + 6;
            cutPosB = filtStr.indexOf("spotlight") - 3;
            chkStr2 = filtStr.substring(cutPosA, cutPosB);
            chkStr2 = chkStr2.replaceAll("\\D+","");
             
            filtStr = filtStr.substring(cutPosC, filtStr.length());
            cutPosA = filtStr.indexOf("description") + 14;

            cutPosB = filtStr.indexOf("currency") - 3;
            desc = filtStr.substring(cutPosA, cutPosB);
            
            cutPosC = filtStr.indexOf("county");
            findPrice = filtStr.substring(0, cutPosC);
            cutPosC = findPrice.indexOf("price");
            
            if ( cutPosC == -1 ) {
                pric = "€1";
            } else {
                cutPosA = filtStr.indexOf("currency")+ 14;
                cutPosB = filtStr.indexOf("county") - 3;
                pric = filtStr.substring(cutPosA, cutPosB);
                temp = pric.replaceAll("[^0-9]+[,]", "");
                if ( temp.indexOf("price") > -1 ) {
                    temp = temp.substring(9, temp.length());
                }
                pric = "€" + temp; 
            }
              
            
                cutPosC = cutPosB;
                filtStr = filtStr.substring(cutPosC, filtStr.length());
              
                cutPosA = filtStr.indexOf("friendlyUrl") + 14;
                cutPosB = filtStr.indexOf("header") - 5;
                
                if ( cutPosB == -1 || cutPosB == -6 ) {
                    cutPosB = filtStr.indexOf("pagingCounts") - 5;
                    cutPosC = cutPosB;
                    flag = true;
                } else {
                    cutPosC = cutPosB + 13;
                }
                
                lnk = filtStr.substring(cutPosA, cutPosB);               
                
                if ( chkStr2.length() <= 2 ) {
                    str2Parse = Integer.parseInt(chkStr2);
                    if ( str2Parse <= 30 ) {
                        t = new Tickets("DoneDeal",pric,desc,lnk);
                        myList.add(t);
                    }                 
                }
                
            }
            
        }
        
        
        return myList;
    }
    
    public List<Jobs> parseDonedealJobs(Elements d) {
        List<Jobs> myList = new ArrayList<>();
        
        Elements dt3 = d.select(".company-name");
        Elements dt4 = d.select(".logo-small");
        Jobs j = null;
        String chk = "";
        Elements e = null;
        
        String title = "";
        String desc = "";
        String lnk = "";
        
        if ( dt3.size() == dt4.size() ) {
            
            for ( int i = 0; i < dt3.size(); i++ ) {
                // this block is for link and description
                    e = dt4.get(i).select("a");
                    lnk = e.attr("href");
                    desc = e.attr("title");

                    lnk = lnk.replaceAll("\\s","");
                // this block is for title
                    e = dt3.get(i).select("a");
                    title = e.html();

                    j = new Jobs(title,desc,lnk);
                    myList.add(j); 
            }
        }
        
        return myList;
    }
}
