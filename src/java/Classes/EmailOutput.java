/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Objects;

/**
 *
 * @author Niall
 */
public class EmailOutput {
    private int idx;
    private String script;

    public EmailOutput(int idx, String script) {
        this.idx = idx;
        this.script = script;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.idx;
        hash = 41 * hash + Objects.hashCode(this.script);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailOutput other = (EmailOutput) obj;
        if (this.idx != other.idx) {
            return false;
        }
        if (!Objects.equals(this.script, other.script)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EmailOutput{" + "idx=" + idx + ", script=" + script + '}';
    }
   
}
