/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;
import javax.mail.Session;

/**
 *
 * @author MajorKingpin
 */
public class EmailBody {
    
    public static ArrayList<EmailOutput> emailoutput = new ArrayList<EmailOutput>();
    static int itrCount = 0;
    
    public void processTickets(List<Tickets> myTicket, String category) {
        
        System.out.println("Processing.....!!");
        String outPut = "";
        EmailOutput eo = null;
        String id = "row" + itrCount;
        
        outPut = "<tr id='" + id + "'>";
                outPut = outPut + "<td style='background-color: yellow;'>    </td>";
                outPut = outPut + "<td style='background-color: yellow;'><b>" + category + "</b></td>";
                outPut = outPut + "<td style='background-color: yellow;'></td>";
        outPut = outPut + "</tr>";
        
        eo = new EmailOutput(itrCount,outPut);
        emailoutput.add(eo);
        itrCount++;
        
        outPut = "";
        for (Tickets t : myTicket ) {
            id = "row" + itrCount;
            outPut = "<tr id='" + id + "'>";
                outPut = outPut + "<td>" + t.getDesc() + "</td>";
                outPut = outPut + "<td><b>" + t.getDomain() + "</b></td>";
                outPut = outPut + "<td><b>" + t.getPrice() + "</b></td>";
                outPut = outPut + "<td>" + t.getLink() + "</td>";
            outPut = outPut + "</tr>";
            eo = new EmailOutput(itrCount,outPut);
            emailoutput.add(eo);
            itrCount++;
        }
        outPut = "";
        
        id = "row" + itrCount;
        outPut = "<tr id='" + id + "'></tr>";
        eo = new EmailOutput(itrCount,outPut);
        emailoutput.add(eo);
        itrCount++;
    }
    
    public void processJobs(List<Jobs> myTicket,String recip) {
        String outPut = "----------------------------------------------"+"\r\n ";
        outPut = outPut + "Reporting Found Jobs :" +"\r\n ";
        outPut = outPut + "--------------------------------------------"+"\r\n ";
        int ct = 0;
        
        for (Jobs t : myTicket) {
            ct++;
            outPut = outPut + " " + ct + ". " + t.getTitle()+ " , " + t.getDesc()+ " , " + t.getLink() + "   " +"\r\n ";    
            outPut = outPut + "----------------------------------------------"+"\r\n ";
        }
        
        outPut = outPut + "-------------------END---------------------------"+"\r\n ";
        
        SendMail sm = new SendMail();
        Session me = sm.auttenticate();
        
        sm.sendEmail(me, outPut, recip);
    }
    
    public static void send(String address) {
        SendMail sm = new SendMail();
        Session me = sm.auttenticate();
        String output = "";
        
        for (EmailOutput eo : emailoutput) {
            output = output + eo.getScript();
        }
        sm.sendEmail(me, output, address); 
    }
}
