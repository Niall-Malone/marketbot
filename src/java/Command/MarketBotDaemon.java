/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;

/**
 *
 * @author Niall
 */
public class MarketBotDaemon implements javax.servlet.ServletContextListener {
    
    public static ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("--------------------------------------");
        System.out.println("Starting MarketBot Service");
        exec.scheduleAtFixedRate(() -> {
            try {
                processTickets.Fetch("YOUR EMAIL HERE!!!");
                System.out.println("Executed!!");
            } catch (InterruptedException ex) {
                Logger.getLogger(MarketBotDaemon.class.getName()).log(Level.SEVERE, null, ex);
            }
        }, 0, 24, TimeUnit.HOURS);  
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Shutting Down MarketBot Service");
        exec.shutdown();
        Enumeration<Driver> drivers = DriverManager.getDrivers();     

        Driver driver = null;
        while(drivers.hasMoreElements()) {
            try {
                driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);

            } catch (SQLException ex) {
                System.out.println("SQL Error " + ex);
            }
        }
    }
}