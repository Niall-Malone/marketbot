/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Classes.EmailBody;
import Classes.Paramaters;
import Classes.ParseData;
import Classes.Tickets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.jsoup.select.Elements;

/**
 *
 * @author Niall
 */
public class processTickets {

    static CleanEmail ce = new CleanEmail();
    static EmailBody eb = new EmailBody();    
    
    public static void Fetch(String address) throws InterruptedException {
        boolean flag = false;
        flag = CleanEmail.clean(address);
        
        ParseData ts = new ParseData();
        String donedeal = "";
        System.out.println("Started Filling Lists");
        List<Tickets> myTickets1 = processTickets(donedeal,ts, "Sport");
        List<Tickets> myTickets2 = processTickets(donedeal,ts, "Music");
        List<Tickets> myTickets3 = processTickets(donedeal,ts, "Comedy");
        System.out.println("Finished Filling Lists");
        
        
        if ( flag == true ) {
            System.out.println("Processing Lists");
            myTickets1 = new ArrayList<Tickets>(new HashSet<Tickets>(myTickets1));
            eb.processTickets(myTickets1, "Sport");
            
            myTickets2 = new ArrayList<Tickets>(new HashSet<Tickets>(myTickets2));
            eb.processTickets(myTickets2, "Music");
            
            myTickets3 = new ArrayList<Tickets>(new HashSet<Tickets>(myTickets3));
            eb.processTickets(myTickets3, "Comedy");
            System.out.println("Finished Processing");
            EmailBody.send(address);
        }        
        
    }
    
    public static ArrayList<Tickets> processTickets(String donedeal, ParseData ts, String categ) throws InterruptedException {
        ArrayList<Tickets> dList = new ArrayList<Tickets>();
        List<String> categoryList = null;
        Paramaters p = new Paramaters();
        Elements e = null;
        int size = 0;
        int donedealSwitch = 0;
        
        switch (categ) {
            case "Sport":
                size = p.sportsReturn().size();
                categoryList = p.sportsReturn();
                donedealSwitch = 3;
                break;
            case "Music":
                size = p.musicReturn().size();
                categoryList = p.musicReturn();
                donedealSwitch = 2;
                break;
            default:
                donedealSwitch = 1;
                size = p.comedyReturn().size();
                categoryList = p.comedyReturn();
                break;
        }
            
        
            for ( int i = 0; i < size; i++ ) { 
                e=ts.getElements(categoryList.get(i));
                dList.addAll(ts.getList(e));
                donedeal = categoryList.get(i);
                donedeal.replaceAll("\\+", "%20");
                e = ts.getElements2(donedeal,donedealSwitch);
                donedeal = e.toString();
                dList.addAll(ts.parseDonedeal(donedeal));
            }
            
        return dList;
    }
}
