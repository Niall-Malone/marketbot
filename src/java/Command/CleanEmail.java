/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author Niall
 */
public class CleanEmail {
    public static boolean clean(String inp) {
        
        if ( !inp.toLowerCase().contains("@") ){
            return false;
        } else if ( inp.toLowerCase().contains("<script")) {
            return false;
        } else if ( inp.toLowerCase().contains("<")) {
            return false;
        } else if ( inp.toLowerCase().contains("*") ) {
            return false;
        }
        
        return true;
    }
}
